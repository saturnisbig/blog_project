#!/usr/bin/env python
# _*_ coding: utf-8 _*_

from django import template
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


register = template.Library()


@register.simple_tag(takes_context=True)
def paginate(context, object_list, page_count):
    left = 1
    right = 1

    paginator = Paginator(object_list, page_count)
    page = context['request'].GET.get('page')

    try:
        article_list = paginator.page(page)
        context['current_page'] = int(page)
        left_page_num = get_left(context['current_page'], left,
                                 paginator.num_pages)
        right_page_num = get_right(context['current_page'], right, paginator.num_pages)
        left_page_num.extend(right_page_num)
        pages = left_page_num
    except PageNotAnInteger:
        article_list = paginator.page(1)
        context['current_page'] = 1
        pages = get_right(context['current_page'], right, paginator.num_pages)
    except EmptyPage:
        article_list = paginator.page(paginator.num_pages)
        context['current_page'] = paginator.num_pages
        pages = get_left(context['current_page'], left, paginator.num_pages)

    pages.sort()
    context['article_list'] = article_list
    context['pages'] = pages
    context['last_page'] = paginator.num_pages
    context['first_page'] = 1
    print(context['pages'])

    try:
        context['pages_first'] = pages[0]
        context['pages_last'] = pages[-1] + 1
    except IndexError:
        context['pages_first'] = 1
        context['pages_last'] = 2

    return ''


def get_left(current_page_num, left_count, total_pages):
    if current_page_num <= 1:
        return []
    else:
        # 排除掉第一页和最后一页
        left = [(current_page_num - 1 - i) for i in range(left_count) if (current_page_num - 1 - i) > 1]
        if current_page_num != total_pages:
            left.append(current_page_num)
        #print(left)
        return left


def get_right(current_page_num, right_count, total_pages):
    if current_page_num == total_pages:
        return []
    else:
        return [(current_page_num + 1 + i) for i in range(right_count) if (current_page_num + 1 + i) < total_pages]
