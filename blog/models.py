# -*- coding: utf-8 -*-

from django.db import models
from django.core.urlresolvers import reverse

from collections import defaultdict

from datetime import datetime


class BlogComment(models.Model):
    user_name = models.CharField('评论者昵称', max_length=100)
    user_email = models.EmailField('评论者邮箱', max_length=255)
    body = models.TextField('评论内容')
    created_time = models.DateTimeField('评论发表时间', auto_now_add=True)
    article = models.ForeignKey('Article', verbose_name='评论所属文章',
                                     on_delete=models.CASCADE)


class ArticleManager(models.Manager):
    
    def archive(self):
        date_list = Article.objects.datetimes('created_time', 'month',
                                             order='DESC')
        date_dict = defaultdict(list)
        for dt in date_list:
            date_dict[dt.year].append(dt.month)
        return date_dict


class Article(models.Model):
    STATUS_CHOICES = (
        ('d', 'Draft'),
        ('p', 'Published'),
    )

    title = models.CharField(u'标题', max_length=70)
    body = models.TextField(u'正文')
    created_time = models.DateTimeField(u'创建时间',)
    last_modified_time = models.DateTimeField(u'修改时间', auto_now=True)
    status = models.CharField(u'文章状态', max_length=1, choices=STATUS_CHOICES)
    abstract = models.CharField(u'摘要', max_length=54, blank=True, null=True,
                                help_text="可选，若为空则自动截取正文的前54个字符")
    views = models.PositiveIntegerField(u'浏览量', default=0)
    likes = models.PositiveIntegerField(u'点赞数', default=0)
    topped = models.BooleanField(u'置顶', default=False)
    category = models.ForeignKey('Category', verbose_name='分类',
                                 null=True,
                                 # 表示删除了某个foreignkey后，对应的文章的该值会被设置为NULL
                                 on_delete=models.SET_NULL)
    tags = models.ManyToManyField('Tag', verbose_name='标签集合', blank=True)

    objects = ArticleManager()

    def __str__(self):
        return self.title

    class Meta:
        ordering = ['-last_modified_time']
        db_tablespace = "tables"
    # 方便获取文章url
    def get_absolute_url(self):
        return reverse('blog:detail', kwargs={'article_id': self.pk})


class Category(models.Model):
    name = models.CharField(u'分类名', max_length=20)
    created_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    last_modified_time = models.DateTimeField(u'修改时间', auto_now=True)

    def __str__(self):
        return self.name


class Tag(models.Model):
    name = models.CharField(u'标签名', max_length=20)
    created_time = models.DateTimeField(u'创建时间', auto_now_add=True)
    last_modified_time = models.DateTimeField(u'修改时间', auto_now=True)

    def __str__(self):
        return self.name
